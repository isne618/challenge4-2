#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>
#include "tree.h"
using namespace std;

void main()
{
	Tree<int>mytree;
	vector<int>total;
	int number, i, num_random, search, deletion, num_delete, insert, num_insert;
	srand(time(0));
	for (i = 0; i < 500; i++) //random 500 numbers
	{
		number = (rand() % 800)+1; //random number 1-800
		mytree.insert(number); //insert numbers to the tree
		total.push_back(number); //push back number to vector
	}
	mytree.inorder(); //output binary tree
	cout << endl;
	cout << "---------------------------------------------------------------------------------------------" << endl;
	cout << "Height of tree is " << mytree.maxDepth() << endl;
	cout << "---------------------------------------------------------------------------------------------" << endl;
	mytree.clear();
	mytree.balance(total,0,total.size()-1);
	cout << "Height of tree after balancing is " << mytree.maxDepth() << endl;
	cout << "---------------------------------------------------------------------------------------------" << endl;
	//allow user to input number of insertion 
	mytree.clear();
	cout << "How many number do you want to random: ";
	cin >> num_random; //input number of random numbers
	for (i = 0; i < num_random; i++)
	{
		number = rand() % 800 + 1; //random 1-800
		mytree.insert(number); //insert number to binary tree
	}
	mytree.inorder();
	cout << endl << "--------------------------------------------------------------------------------------" << endl;
	cout << "How many 'INSERTION' operations should be performed: ";
	cin >> insert; //input number of insertion 
	for (int i = 0; i < insert; i++) //loop to input number that you want to add
	{
		cout <<" Input number that you want to insert: ";
		cin >> num_insert;
		mytree.insert(num_insert); //insert number to binary tree
	}
	cout << "----------------------------------------------------------------------------------------------" << endl;
	mytree.inorder();
	cout << endl;
	cout << "----------------------------------------------------------------------------------------------" << endl;
	cout << "Height of tree is " << mytree.maxDepth() << endl;
	cout << "----------------------------------------------------------------------------------------------" << endl;
	cout << "Input number that you want to search: ";
	cin >> search;
	if (mytree.search(search))
	{
		cout << search << " found!!" << endl;
	}
	else
	{
		cout << search << " not found!!" << endl;
	}
	cout << "----------------------------------------------------------------------------------------------" << endl;
	cout << "How many 'DELETION' operations should be performed: ";
	cin >> deletion;
	cout << "----------------------------------------------------------------------------------------------" << endl;
	i = 0;
	while (i < deletion)
	{
		num_delete = rand() % 800 + 1;
		if (mytree.search(num_delete) == false)
		{
			cout << "Delete --> " << num_delete << " --> This number is not in the tree." << endl;
			i++;
			continue;
		}
		if (mytree.search(num_delete) == true)
		{
			mytree.search_delete(num_delete);
			cout << "Delete --> " << num_delete << " --> was success deleted." << endl;
		}
		i++;
	}
	cout << endl;
	cout << "**After deleted**" << endl;
	cout << "----------------------------------------------------------------------------------------------" << endl;
	mytree.inorder();
	cout << endl;
	cout << "----------------------------------------------------------------------------------------------" << endl;
	cout << "Input number that you want to search: ";
	cin >> search;
	if (mytree.search(search))
	{
		cout << search << " found!!" << endl;
	}
	else
	{
		cout << search << " not found!!" << endl;
	}
	cout << "----------------------------------------------------------------------------------------------" << endl;
}