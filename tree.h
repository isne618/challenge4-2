#include <queue>
#include <stack>
using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node {
public:
	Node() { left = right = NULL; }
	Node(const T& el, Node* l = 0, Node * r = 0) {
		key = el; left = l; right = r;
	}
	T key;
	Node* left, * right;
};

template<class T>
class Tree {
public:
	Tree() { root = 0; }
	~Tree() { clear(); }
	void clear() { clear(root); root = 0; }
	bool isEmpty() { return root == 0; }
	void inorder() { inorder(root); }
	int maxDepth() { return maxDepth(root); }
	void insert(const T& el);
	void deleteNode(Node<T>*& node);
	void visit(Node<T>* p);
	void balance(vector<T>data, int first, int last);
	bool search(const T& el);
	void search_delete(T key) { search_delete(root, key); }
	
protected:
	Node<T>* root;
	int maxDepth(Node<T>* p);
	void clear(Node<T>* p);
	void inorder(Node<T>* p);
	void search_delete(Node<T>* p, int num_delete);
	
};

template<class T>
void Tree<T>::clear(Node<T>* p)
{
	if (p != 0) {
		clear(p->left);
		clear(p->right);
		delete p;
	}
}

template<class T>
void Tree<T>::inorder(Node<T>* p) {
	//TO DO! This is for an inorder tree traversal!
	if (p != 0) {
		inorder(p->left);
		visit(p); //output the tree
		inorder(p->right);
	}
}

template<class T>
void Tree<T>::search_delete(Node<T>* p, int num_delete)
{
	Node<T>* tmp = p, * prev = 0; 
	while (tmp != 0)
	{
		if (tmp->key == num_delete)  
			break;
		prev = tmp; 
		if (tmp->key < num_delete) 
			tmp = tmp->right;
		else
			tmp = tmp->left; 
	}
	if (tmp != 0 && tmp->key == num_delete)
	{
		if (tmp == p) 
			deleteNode(p); 
		else if (prev->left == tmp) 
			deleteNode(prev->left);
		else
			deleteNode(prev->right);
	}
	else if (p != 0) 
		cout << "Number " << num_delete << " was not found ";
	else
		cout << "Tree is Empty! ";
}

template<class T>
void Tree<T>::insert(const T& el) {
	Node<T>* p = root, * prev = 0;
	while (p != 0) {
		prev = p;
		if (p->key < el)
			p = p->right;
		else
			p = p->left;
	}
	if (root == 0)
		root = new Node<T>(el);
	else if (prev->key < el)
		prev->right = new Node<T>(el);
	else
		prev->left = new Node<T>(el);
}

template<class T>
void Tree<T>::deleteNode(Node<T>*& node) {
	Node<T>* prev, * tmp = node;
	if (node->right == 0)
		node = node->left;
	else if (node->left == 0)
		node = node->right;
	else {
		tmp = node->left;
		prev = node;
		while (tmp->right != 0) {
			prev = tmp;
			tmp = tmp->right;
		}
		node->key = tmp->key;
		if (prev == node)
			prev->left = tmp->left;
		else prev->right = tmp->left;
	}
	delete tmp;
}

template<class T>
int Tree<T>::maxDepth(Node<T>* p)
{

	if (p == NULL)
		return 0; 
	else
	{
		// compute the depth of each subtree 
		int left_Depth = maxDepth(p->left);
		int right_Depth = maxDepth(p->right);

		// use the larger one 
		if (left_Depth > right_Depth)
		{
			return(left_Depth + 1);
		}
		else
		{
			return(right_Depth + 1);
		}
	}
}

template<class T>
void Tree<T>::balance(vector<T>data, int first, int last) {
	if (first <= last) {
		int middle = (first + last) / 2;
		insert(data[middle]);
		balance(data, first, middle - 1);
		balance(data, middle + 1, last);
	}
}

template<class T>
bool Tree<T>::search(const T& el) {
	Node<T>* p = root;
	while (p != 0)
		if (el == p->key)
			return true;
		else if (el < p->key)
			p = p->left;
		else
			p = p->right;
	return false;
}



template<class T>
void Tree<T>::visit(Node<T>* p)
{
	cout << setw(3) << p->key << " "; //cout key of the node 
}

#endif // Binary_Search_Tree